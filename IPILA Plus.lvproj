﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Installer Files" Type="Folder" URL="../Installer Files">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="subvis" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="convert pressure to spl.vi" Type="VI" URL="../sub vis/convert pressure to spl.vi"/>
			<Item Name="generate icil third ob export block.vi" Type="VI" URL="../sub vis/generate icil third ob export block.vi"/>
			<Item Name="estimated occluded functional global.vi" Type="VI" URL="../sub vis/estimated occluded functional global.vi"/>
			<Item Name="export estimated occluded to tdms.vi" Type="VI" URL="../sub vis/export estimated occluded to tdms.vi"/>
			<Item Name="Export ICIL Level.vi" Type="VI" URL="../sub vis/Export ICIL Level.vi"/>
			<Item Name="export ICIL.vi" Type="VI" URL="../sub vis/export ICIL.vi"/>
			<Item Name="generate ICIL export header block.vi" Type="VI" URL="../sub vis/generate ICIL export header block.vi"/>
			<Item Name="ICIL Bone Cond Functional Global.vi" Type="VI" URL="../sub vis/ICIL Bone Cond Functional Global.vi"/>
			<Item Name="ICIL Functional Global.vi" Type="VI" URL="../sub vis/ICIL Functional Global.vi"/>
			<Item Name="tdms viewer.vi" Type="VI" URL="../sub vis/tdms viewer.vi"/>
			<Item Name="bone conduction functional global.vi" Type="VI" URL="../sub vis/bone conduction functional global.vi"/>
			<Item Name="generate ICIL freq list.vi" Type="VI" URL="../sub vis/generate ICIL freq list.vi"/>
			<Item Name="ipila standards splash screen.vi" Type="VI" URL="../sub vis/ipila standards splash screen.vi"/>
			<Item Name="plot zero bone cond phase.vi" Type="VI" URL="../sub vis/plot zero bone cond phase.vi"/>
			<Item Name="Test Types for Waveform Comparison.ctl" Type="VI" URL="../sub vis/IPILA Plus.llb/Test Types for Waveform Comparison.ctl"/>
			<Item Name="Wave Selection Controls for Waveform Comparison.ctl" Type="VI" URL="../sub vis/IPILA Plus.llb/Wave Selection Controls for Waveform Comparison.ctl"/>
		</Item>
		<Item Name="IPILA Plus.vi" Type="VI" URL="../sub vis/IPILA Plus.llb/IPILA Plus.vi"/>
		<Item Name="IPILA Plus.lvlib" Type="Library" URL="../sub vis/IPILA Plus.llb/IPILA Plus.lvlib"/>
		<Item Name="peak pass fail set overall.vi" Type="VI" URL="../sub vis/peak pass fail set overall.vi"/>
		<Item Name="icil or ictf.vi" Type="VI" URL="../sub vis/icil or ictf.vi"/>
		<Item Name="icil or ictf phase.vi" Type="VI" URL="../sub vis/icil or ictf phase.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="_2DArrToArrWfms.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_2DArrToArrWfms.vi"/>
				<Item Name="_Get Sound Error From Return Value.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_Get Sound Error From Return Value.vi"/>
				<Item Name="AsciiToInt.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/AsciiToInt.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check for multiple of dt.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for multiple of dt.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Clear-68016.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/Clear-68016.vi"/>
				<Item Name="ClearError.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ClearError.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="configureNumberOfValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/configureNumberOfValues.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Digital Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Subset.vi"/>
				<Item Name="DTbl Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Empty Digital.vi"/>
				<Item Name="DTbl Get Digital Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Get Digital Value.vi"/>
				<Item Name="DU64_U32SubtractWithBorrow.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/TSOps.llb/DU64_U32SubtractWithBorrow.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DWDT Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Subset.vi"/>
				<Item Name="DWDT Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get XY Value.vi"/>
				<Item Name="DWDT Waveform Duration.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Waveform Duration.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="ExtractSubstring.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ExtractSubstring.vi"/>
				<Item Name="fileViewerConfigData.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/fileViewerConfigData.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="formatPropertyList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/formatPropertyList.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Report Object Reference.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Generate Report Object Reference.ctl"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get Type of Variant.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Get Type of Variant.vi"/>
				<Item Name="Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Subset.vi"/>
				<Item Name="Get XY Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get XY Value.vi"/>
				<Item Name="getChannelList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/getChannelList.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="getNamesFromPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/getNamesFromPath.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GoTo.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/GoTo.vi"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="I128 Timestamp.ctl" Type="VI" URL="/&lt;vilib&gt;/Waveform/TSOps.llb/I128 Timestamp.ctl"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="initFileContentsTree.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initFileContentsTree.vi"/>
				<Item Name="InitFromConfiguration.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/InitFromConfiguration.vi"/>
				<Item Name="initHelpButtonVisibility.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initHelpButtonVisibility.vi"/>
				<Item Name="InitScrollbarAndListBox.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/InitScrollbarAndListBox.vi"/>
				<Item Name="initTabValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/initTabValues.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="loadAndFormatValues.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/loadAndFormatValues.vi"/>
				<Item Name="LoadBufferForMultiListBoxAndFormat.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/LoadBufferForMultiListBoxAndFormat.vi"/>
				<Item Name="LogicalSort.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/LogicalSort.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="Multiplot Octave Graph.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Controls/subVIs/Multiplot Octave Graph.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="NI_MAPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MAPro.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="panelResize_tdms.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/panelResize_tdms.vi"/>
				<Item Name="panelstate.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/panelstate.ctl"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="setListBoxColumnWidths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/setListBoxColumnWidths.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="sizeaction.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/sizeaction.ctl"/>
				<Item Name="Sound Data Format.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Data Format.ctl"/>
				<Item Name="Sound File Close.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Close.vi"/>
				<Item Name="Sound File Info (path).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info (path).vi"/>
				<Item Name="Sound File Info (refnum).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info (refnum).vi"/>
				<Item Name="Sound File Info.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info.vi"/>
				<Item Name="Sound File Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Open.vi"/>
				<Item Name="Sound File Position.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Position.ctl"/>
				<Item Name="Sound File Read (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (DBL).vi"/>
				<Item Name="Sound File Read (I16).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (I16).vi"/>
				<Item Name="Sound File Read (I32).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (I32).vi"/>
				<Item Name="Sound File Read (SGL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (SGL).vi"/>
				<Item Name="Sound File Read (U8).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (U8).vi"/>
				<Item Name="Sound File Read Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read Open.vi"/>
				<Item Name="Sound File Read Simple.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read Simple.vi"/>
				<Item Name="Sound File Read.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read.vi"/>
				<Item Name="Sound File Refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Refnum.ctl"/>
				<Item Name="Sound File Write Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write Open.vi"/>
				<Item Name="SoundVib_ABCWeighting.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/SoundVib_ABCWeighting.lvlib"/>
				<Item Name="SoundVib_Octave.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Octave/SoundVib_Octave.lvlib"/>
				<Item Name="SoundVib_SoundLevel.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/SoundVib_SoundLevel.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="status.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/status.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="sv_A Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_A Weighting Filter.vi"/>
				<Item Name="sv_B Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_B Weighting Filter.vi"/>
				<Item Name="sv_C Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_C Weighting Filter.vi"/>
				<Item Name="sv_C-Message Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_C-Message Weighting Filter.vi"/>
				<Item Name="sv_CCITT Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_CCITT Weighting Filter.vi"/>
				<Item Name="sv_Check Data Valid (N Ch) [Weighting].vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Check Data Valid (N Ch) [Weighting].vi"/>
				<Item Name="sv_Check Data Valid [Weighting].vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Check Data Valid [Weighting].vi"/>
				<Item Name="sv_Check for Weighting Change (time signal).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Check for Weighting Change (time signal).vi"/>
				<Item Name="sv_Check for Weighting Conflict (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Check for Weighting Conflict (N Ch).vi"/>
				<Item Name="sv_Check for Weighting Conflict.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Check for Weighting Conflict.vi"/>
				<Item Name="sv_Coefficients for Exponential Averaging.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/sv_Coefficients for Exponential Averaging.vi"/>
				<Item Name="sv_Dolby Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Dolby Weighting Filter.vi"/>
				<Item Name="sv_Equivalent Continuous Level with IC_octave.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/sv_Equivalent Continuous Level with IC_octave.vi"/>
				<Item Name="sv_Exp Avg Level with IC (final value)_octave.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/sv_Exp Avg Level with IC (final value)_octave.vi"/>
				<Item Name="sv_Get Audio Weighting Filter Coefficients.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Get Audio Weighting Filter Coefficients.vi"/>
				<Item Name="sv_Get Index for Unit label.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/sv_Get Index for Unit label.vi"/>
				<Item Name="sv_Impulse Peak Detector with IC (final value)_octave.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/sv_Impulse Peak Detector with IC (final value)_octave.vi"/>
				<Item Name="sv_ITU-R 468-4 Weighting Filter.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_ITU-R 468-4 Weighting Filter.vi"/>
				<Item Name="sv_Peak Level with IC_octave.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/sv_Peak Level with IC_octave.vi"/>
				<Item Name="sv_Weighting Filter with IC.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Weighting/subVIs/sv_Weighting Filter with IC.vi"/>
				<Item Name="svc_Basic Datatype Defaults.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Basic Datatype Defaults.vi"/>
				<Item Name="svc_Caller VI Returned in Error Source.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Caller VI Returned in Error Source.vi"/>
				<Item Name="svc_Channel Info.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Controls/svc_Channel Info.ctl"/>
				<Item Name="svc_Check for Waveform Parameter Continuity (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Check for Waveform Parameter Continuity (1 Ch).vi"/>
				<Item Name="svc_Check for Waveform Parameter Continuity (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Check for Waveform Parameter Continuity (N Ch).vi"/>
				<Item Name="svc_Check for Waveform Parameter Continuity (no state).vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Check for Waveform Parameter Continuity (no state).vi"/>
				<Item Name="svc_Engineering Units Text Ring.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Controls/svc_Engineering Units Text Ring.ctl"/>
				<Item Name="svc_Engineering Units to Unit Label.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Engineering Units to Unit Label.vi"/>
				<Item Name="svc_Extract Unit Label (channel info).vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Extract Unit Label (channel info).vi"/>
				<Item Name="svc_Get SV Channel Info.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Get SV Channel Info.vi"/>
				<Item Name="svc_Get Weighting Enum and Labels.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Get Weighting Enum and Labels.vi"/>
				<Item Name="svc_Octave Info.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Controls/svc_Octave Info.ctl"/>
				<Item Name="svc_Octave Standard.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Controls/svc_Octave Standard.ctl"/>
				<Item Name="svc_Set SV Channel Info (variant).vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Set SV Channel Info (variant).vi"/>
				<Item Name="svc_Set SV Channel Info.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Set SV Channel Info.vi"/>
				<Item Name="svc_Unit Label Lexical Class.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Unit Label Lexical Class.vi"/>
				<Item Name="svc_Unit Label Manipulation.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Unit Label Manipulation.vi"/>
				<Item Name="svc_Unit Label to Engineering Units.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Unit Label to Engineering Units.vi"/>
				<Item Name="svc_Waveform Parameters.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Controls/svc_Waveform Parameters.ctl"/>
				<Item Name="svc_Weighting Filter Text Ring.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Controls/svc_Weighting Filter Text Ring.ctl"/>
				<Item Name="svc_Weighting to Weighting Label.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Weighting to Weighting Label.vi"/>
				<Item Name="SVL Set dB Reference (variant).vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/SVL Set dB Reference (variant).vi"/>
				<Item Name="svl_Build dB Label (channel info).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Build dB Label (channel info).vi"/>
				<Item Name="svl_Check Data Valid (N Ch) [SLM].vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Check Data Valid (N Ch) [SLM].vi"/>
				<Item Name="svl_Check Data Valid [SLM].vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Check Data Valid [SLM].vi"/>
				<Item Name="svl_Check Decimation Factor.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Check Decimation Factor.vi"/>
				<Item Name="svl_Check Exp Mode and Time Constant.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Check Exp Mode and Time Constant.vi"/>
				<Item Name="svl_Check Integration Time.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Check Integration Time.vi"/>
				<Item Name="svl_Coefficients for Exponential Averaging.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Coefficients for Exponential Averaging.vi"/>
				<Item Name="svl_Decimate Signal (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Decimate Signal (1 Ch).vi"/>
				<Item Name="svl_Decimate Signal (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Decimate Signal (N Ch).vi"/>
				<Item Name="svl_Decimated Exp Avg Level (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Decimated Exp Avg Level (1 Ch).vi"/>
				<Item Name="svl_Decimated Exp Avg Level (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Decimated Exp Avg Level (N Ch).vi"/>
				<Item Name="svl_Decimation with IC (WDT).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Decimation with IC (WDT).vi"/>
				<Item Name="svl_Equivalent Continuous Level (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Equivalent Continuous Level (1 Ch).vi"/>
				<Item Name="svl_Equivalent Continuous Level (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Equivalent Continuous Level (N Ch).vi"/>
				<Item Name="svl_Equivalent Continuous Level with IC.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Equivalent Continuous Level with IC.vi"/>
				<Item Name="svl_Exp Avg Level (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Exp Avg Level (1 Ch).vi"/>
				<Item Name="svl_Exp Avg Level (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Exp Avg Level (N Ch).vi"/>
				<Item Name="svl_Exp Avg Level Core (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Exp Avg Level Core (N Ch).vi"/>
				<Item Name="svl_Exponential Filter with IC.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Exponential Filter with IC.vi"/>
				<Item Name="svl_Impulse Peak Detector with IC.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Impulse Peak Detector with IC.vi"/>
				<Item Name="svl_Init Arrays for Exp Avg.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Init Arrays for Exp Avg.vi"/>
				<Item Name="svl_Level Units.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Level Units.vi"/>
				<Item Name="svl_Log View for Energy Level (N Ch) (array).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Log View for Energy Level (N Ch) (array).vi"/>
				<Item Name="svl_Log View for Energy Level (N Ch) (single value).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Log View for Energy Level (N Ch) (single value).vi"/>
				<Item Name="svl_Log View for Energy Level (single value).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Log View for Energy Level (single value).vi"/>
				<Item Name="svl_Log View for Energy Level (WDT).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Log View for Energy Level (WDT).vi"/>
				<Item Name="svl_Log View for Peak Level (N Ch) (single value).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Log View for Peak Level (N Ch) (single value).vi"/>
				<Item Name="svl_Log View for Peak Level (single value).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Log View for Peak Level (single value).vi"/>
				<Item Name="svl_Peak Level (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Peak Level (1 Ch).vi"/>
				<Item Name="svl_Peak Level (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Peak Level (N Ch).vi"/>
				<Item Name="svl_Peak Level with IC.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Peak Level with IC.vi"/>
				<Item Name="svl_Running Equivalent Continuous Sound Level (1 Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Running Equivalent Continuous Sound Level (1 Ch).vi"/>
				<Item Name="svl_Running Equivalent Continuous Sound Level (N Ch).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Level/subVIs/svl_Running Equivalent Continuous Sound Level (N Ch).vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TDMS - File Viewer.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMS - File Viewer.vi"/>
				<Item Name="TDMSFileViewer_LaunchHelp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMSFileViewer_LaunchHelp.vi"/>
				<Item Name="TDMSFileViewerLocalizedText.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/TDMSFileViewerLocalizedText.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Timestamp Subtract.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/TSOps.llb/Timestamp Subtract.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Type Enum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/Type Enum.ctl"/>
				<Item Name="UpdateBufferForMultiListBoxIfNecessary.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateBufferForMultiListBoxIfNecessary.vi"/>
				<Item Name="UpdateListBoxAfterKeyEvent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateListBoxAfterKeyEvent.vi"/>
				<Item Name="UpdateScrollbarBeforeKeyEvent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/UpdateScrollbarBeforeKeyEvent.vi"/>
				<Item Name="VariantType to Type Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/GetType.llb/VariantType to Type Code.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="Waveform Duration.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Waveform Duration.vi"/>
				<Item Name="Waveform Min Max.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Waveform Min Max.vi"/>
				<Item Name="Waveform Scale and Offset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Waveform Scale and Offset.vi"/>
				<Item Name="WDT Get Waveform Subset CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset CDB.vi"/>
				<Item Name="WDT Get Waveform Subset DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset DBL.vi"/>
				<Item Name="WDT Get Waveform Subset EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset EXT.vi"/>
				<Item Name="WDT Get Waveform Subset I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I8.vi"/>
				<Item Name="WDT Get Waveform Subset I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I16.vi"/>
				<Item Name="WDT Get Waveform Subset I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I32.vi"/>
				<Item Name="WDT Get Waveform Subset SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset SGL.vi"/>
				<Item Name="WDT Get XY Value CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CDB.vi"/>
				<Item Name="WDT Get XY Value CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value CXT.vi"/>
				<Item Name="WDT Get XY Value DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value DBL.vi"/>
				<Item Name="WDT Get XY Value EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value EXT.vi"/>
				<Item Name="WDT Get XY Value I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I16.vi"/>
				<Item Name="WDT Get XY Value I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I32.vi"/>
				<Item Name="WDT Get XY Value I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get XY Value I64.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="WDT Waveform Duration DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Waveform Duration DBL.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write GIF File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/gif.llb/Write GIF File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="SoundVib_Octave_2014 standard.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/Sound and Vibration/svt_Octave/SoundVib_Octave_2014 standard.lvlib"/>
				<Item Name="Excel _Workbook Node NO WorkIdentity.lvlibp" Type="LVLibp" URL="/&lt;vilib&gt;/addons/_office/_exclsub.llb/Excel _Workbook Node NO WorkIdentity.lvlibp">
					<Item Name="Excel _Workbook Node NO WorkIdentity.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_office/_exclsub.llb/Excel _Workbook Node NO WorkIdentity.lvlibp/Excel _Workbook Node NO WorkIdentity.vi"/>
				</Item>
				<Item Name="svc_Create X Array from x0 dx and length.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_NISVFA/_Shared subVIs/Common/svc_Create X Array from x0 dx and length.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Band Number to Third OB Graph.vi" Type="VI" URL="../sub vis/FFT to OB Folder/E1050 LV801 Source Code 2007 JUL 13/lv2013 code Subfolder 2014 APR 17/Band Number to Third OB Graph.vi"/>
			<Item Name="Bone Conduction Lookup v4 DN LV2015.vi" Type="VI" URL="../sub vis/Bone Conduction Lookup v4 DN LV2015.vi"/>
			<Item Name="Fractional-octave time domain analysis (N Ch) (v2).vi" Type="VI" URL="../1103 EAR IPILA v3 LabVIEW/13OB Duration Calculators/Intensity/Fractional-octave time domain analysis (N Ch) (v2).vi"/>
			<Item Name="Frequency Domain Multiplication.vi" Type="VI" URL="../sub vis/Frequency Domain Multiplication.vi"/>
			<Item Name="Friedlander Blast Wave.vi" Type="VI" URL="../sub vis/Friedlander Blast Wave.vi"/>
			<Item Name="gras_Fractional-octave Analysis (N Ch).vi" Type="VI" URL="../1103 EAR IPILA v3 LabVIEW/gras_Fractional-octave Analysis (N Ch).vi"/>
			<Item Name="gras_Fractional-octave Analysis with IC (linear avg).vi" Type="VI" URL="../Intensity VIs/gras_Fractional-octave Analysis with IC (linear avg).vi"/>
			<Item Name="gras_Fractional-octave Analysis with IC (linear mode).vi" Type="VI" URL="../Intensity VIs/gras_Fractional-octave Analysis with IC (linear mode).vi"/>
			<Item Name="gras_Fractional-octave Analysis with IC (peak mode).vi" Type="VI" URL="../Intensity VIs/gras_Fractional-octave Analysis with IC (peak mode).vi"/>
			<Item Name="gras_Fractional-octave Analysis with IC.vi" Type="VI" URL="../Intensity VIs/gras_Fractional-octave Analysis with IC.vi"/>
			<Item Name="ICIL display first halg.vi" Type="VI" URL="../sub vis/ICIL display first halg.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="load bg noise wav file sub vi.vi" Type="VI" URL="../sub vis/load bg noise wav file sub vi.vi"/>
			<Item Name="load bg noise wav file.vi" Type="VI" URL="../sub vis/load bg noise wav file.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="lvsound2.dll" Type="Document" URL="/&lt;resource&gt;/lvsound2.dll"/>
			<Item Name="Magnitude and Phase Averager.vi" Type="VI" URL="../sub vis/Magnitude and Phase Averager.vi"/>
			<Item Name="Convert WAV Data to Scaled Waveform NAE B&amp;K dBA 2018 JAN 24 Wfm.vi" Type="VI" URL="../sub vis/Convert WAV Data to Scaled Waveform NAE B&amp;K dBA 2018 JAN 24 Wfm.vi"/>
			<Item Name="dB10Log.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/dB10Log.vi"/>
			<Item Name="Log F Interpolation.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/Log F Interpolation.vi"/>
			<Item Name="Bone Conduction Lookup v8 DN LV2015.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/Bone Conduction Lookup v8 DN LV2015.vi"/>
			<Item Name="Un-dB and determine Line NUmber.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/Un-dB and determine Line NUmber.vi"/>
			<Item Name="13OB Smoothing.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/13OB Smoothing.vi"/>
			<Item Name="Convert MAG FRF to two sided icil transfer function LV2015 v3.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/Convert MAG FRF to two sided icil transfer function LV2015 v3.vi"/>
			<Item Name="ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31.vi"/>
			<Item Name="1-Sided FFT Frequency Generator.vi" Type="VI" URL="../sub vis/ICIL as TF Add Bone Conduction Smoothed (Complex) 2018 JUL 31 Folder/1-Sided FFT Frequency Generator.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="IPILA Plus" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{74D6DAEC-1975-4B37-857B-C7A942C515DE}</Property>
				<Property Name="App_INI_GUID" Type="Str">{52AE7FB5-8EC0-43F9-93D2-2F80F3F46D8B}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{76E3079E-7C3E-4A4D-8F7B-8393D08F2350}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">IPILA Plus</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/IPILA Plus 1.1.1.exe</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C5289C4F-8799-44AF-A7A5-2CE7AC9A2A23}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">1</Property>
				<Property Name="Bld_version.patch" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">IPILA Plus 1.1.1.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/IPILA Plus 1.1.1.exe/IPILA Plus 1.0.2.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/IPILA Plus 1.1.1.exe/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/Installer Files/favicon(18).ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{25920BC9-79AA-4C24-A032-F4EE73255041}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/IPILA Plus.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Nelson Acoustics</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">IPILA Plus 1.1.1</Property>
				<Property Name="TgtF_internalName" Type="Str">IPILA Plus 1.1.1</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 Nelson Acoustics</Property>
				<Property Name="TgtF_productName" Type="Str">IPILA Plus</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{C28BD80F-A5E1-49E8-95F9-D7DFC4B53E4C}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">IPILA Plus 1.1.1.exe</Property>
			</Item>
			<Item Name="IPILA Plus Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">VI Acoustics</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{143C22D3-2EE6-4745-829D-87DA0114AA06}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">IPILA Plus 1.1.1</Property>
				<Property Name="Destination[1].parent" Type="Str">{143C22D3-2EE6-4745-829D-87DA0114AA06}</Property>
				<Property Name="Destination[1].tag" Type="Str">{0EC7591A-8297-4E2E-8A68-9D4F8B5598C0}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="Destination[2].name" Type="Str">IPILA Plus</Property>
				<Property Name="Destination[2].parent" Type="Str">{7C5E53B9-0CC8-49B4-8873-46C4C66A2706}</Property>
				<Property Name="Destination[2].tag" Type="Str">{936B0C86-FA68-492A-93EE-6F050131ADCA}</Property>
				<Property Name="Destination[2].type" Type="Str">userFolder</Property>
				<Property Name="Destination[3].name" Type="Str">Data</Property>
				<Property Name="Destination[3].parent" Type="Str">{936B0C86-FA68-492A-93EE-6F050131ADCA}</Property>
				<Property Name="Destination[3].tag" Type="Str">{AB796B14-87F7-453F-9AF5-B015D0875ACC}</Property>
				<Property Name="Destination[3].type" Type="Str">userFolder</Property>
				<Property Name="Destination[4].name" Type="Str">Real Protector Example Set</Property>
				<Property Name="Destination[4].parent" Type="Str">{AB796B14-87F7-453F-9AF5-B015D0875ACC}</Property>
				<Property Name="Destination[4].tag" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Destination[4].type" Type="Str">userFolder</Property>
				<Property Name="Destination[5].name" Type="Str">Friedlander Impulse 10 dB Attenuation</Property>
				<Property Name="Destination[5].parent" Type="Str">{AB796B14-87F7-453F-9AF5-B015D0875ACC}</Property>
				<Property Name="Destination[5].tag" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Destination[5].type" Type="Str">userFolder</Property>
				<Property Name="Destination[6].name" Type="Str">Friedlander Impulse 60 dB Attenuation</Property>
				<Property Name="Destination[6].parent" Type="Str">{AB796B14-87F7-453F-9AF5-B015D0875ACC}</Property>
				<Property Name="Destination[6].tag" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Destination[6].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">7</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{1A304EEE-52F4-4217-A14E-A1B409FA933E}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Runtime 2018 f2</Property>
				<Property Name="DistPart[0].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[0].productName" Type="Str">NI LabVIEW Runtime 2018 Non-English Support.</Property>
				<Property Name="DistPart[0].SoftDep[0].upgradeCode" Type="Str">{3C68D03D-EF38-41B5-9977-E27520759DD6}</Property>
				<Property Name="DistPart[0].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[0].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[0].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[10].productName" Type="Str">NI mDNS Responder 17.0</Property>
				<Property Name="DistPart[0].SoftDep[10].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[0].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[11].productName" Type="Str">NI Deployment Framework 2018</Property>
				<Property Name="DistPart[0].SoftDep[11].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[0].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[12].productName" Type="Str">NI Error Reporting 2018</Property>
				<Property Name="DistPart[0].SoftDep[12].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[0].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[2].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[0].SoftDep[2].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[0].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[3].productName" Type="Str">Math Kernel Libraries 2018</Property>
				<Property Name="DistPart[0].SoftDep[3].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[0].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[4].productName" Type="Str">NI Logos 18.0</Property>
				<Property Name="DistPart[0].SoftDep[4].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[0].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[5].productName" Type="Str">NI TDM Streaming 18.0</Property>
				<Property Name="DistPart[0].SoftDep[5].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[0].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2018</Property>
				<Property Name="DistPart[0].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[0].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[7].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2018</Property>
				<Property Name="DistPart[0].SoftDep[7].upgradeCode" Type="Str">{EF4708F6-5A34-4DBA-B12B-79CC2004E20B}</Property>
				<Property Name="DistPart[0].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[8].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[0].SoftDep[8].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[0].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[9].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[0].SoftDep[9].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[0].SoftDepCount" Type="Int">13</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{3B195EBF-4A09-46E6-8EAD-931568C1344C}</Property>
				<Property Name="DistPartCount" Type="Int">1</Property>
				<Property Name="INST_author" Type="Str">Microsoft</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/IPILA Plus Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">IPILA Plus Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{0EC7591A-8297-4E2E-8A68-9D4F8B5598C0}</Property>
				<Property Name="INST_productName" Type="Str">IPILA Plus</Property>
				<Property Name="INST_productVersion" Type="Str">1.1.1</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">18008012</Property>
				<Property Name="MSI_arpCompany" Type="Str">VI Acoustics</Property>
				<Property Name="MSI_arpURL" Type="Str">www.viacoustics.com</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{8B7321E5-934C-43E2-AE5B-B744975621D3}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{89E241F4-421A-491D-A948-06D0D0784CCC}</Property>
				<Property Name="MSI_windowTitle" Type="Str">IPILA Plus Installer</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{0EC7591A-8297-4E2E-8A68-9D4F8B5598C0}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{0EC7591A-8297-4E2E-8A68-9D4F8B5598C0}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">IPILA Plus 1.1.1.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">1</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">IPILA Plus 1.1.1</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str"></Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{C28BD80F-A5E1-49E8-95F9-D7DFC4B53E4C}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">IPILA Plus</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/IPILA Plus</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="Source[1].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[1].name" Type="Str">Test3_Pcal,1,132_A1_ N134.wav</Property>
				<Property Name="Source[1].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,1,132_A1_ N134.wav</Property>
				<Property Name="Source[1].type" Type="Str">File</Property>
				<Property Name="Source[10].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[10].name" Type="Str">Test3_Pcal,3,150_A1_ N154.wav</Property>
				<Property Name="Source[10].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,3,150_A1_ N154.wav</Property>
				<Property Name="Source[10].type" Type="Str">File</Property>
				<Property Name="Source[100].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[100].name" Type="Str">Test_Ptest,5,1,132_.wav</Property>
				<Property Name="Source[100].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,5,1,132_.wav</Property>
				<Property Name="Source[100].type" Type="Str">File</Property>
				<Property Name="Source[101].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[101].name" Type="Str">Test_Ptest,5,1,150_.wav</Property>
				<Property Name="Source[101].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,5,1,150_.wav</Property>
				<Property Name="Source[101].type" Type="Str">File</Property>
				<Property Name="Source[102].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[102].name" Type="Str">Test_Ptest,5,1,168_.wav</Property>
				<Property Name="Source[102].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,5,1,168_.wav</Property>
				<Property Name="Source[102].type" Type="Str">File</Property>
				<Property Name="Source[103].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[103].name" Type="Str">Test_Ptest,5,2,132_.wav</Property>
				<Property Name="Source[103].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,5,2,132_.wav</Property>
				<Property Name="Source[103].type" Type="Str">File</Property>
				<Property Name="Source[104].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[104].name" Type="Str">Test_Ptest,5,2,150_.wav</Property>
				<Property Name="Source[104].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,5,2,150_.wav</Property>
				<Property Name="Source[104].type" Type="Str">File</Property>
				<Property Name="Source[105].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[105].name" Type="Str">Test_Ptest,5,2,168_.wav</Property>
				<Property Name="Source[105].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,5,2,168_.wav</Property>
				<Property Name="Source[105].type" Type="Str">File</Property>
				<Property Name="Source[106].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[106].name" Type="Str">noisefloor_Pcal,bg,132_.wav</Property>
				<Property Name="Source[106].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/noisefloor_Pcal,bg,132_.wav</Property>
				<Property Name="Source[106].type" Type="Str">File</Property>
				<Property Name="Source[107].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[107].name" Type="Str">noisefloor_Pcal,bg,150_.wav</Property>
				<Property Name="Source[107].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/noisefloor_Pcal,bg,150_.wav</Property>
				<Property Name="Source[107].type" Type="Str">File</Property>
				<Property Name="Source[108].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[108].name" Type="Str">noisefloor_Pcal,bg,168_.wav</Property>
				<Property Name="Source[108].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/noisefloor_Pcal,bg,168_.wav</Property>
				<Property Name="Source[108].type" Type="Str">File</Property>
				<Property Name="Source[109].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[109].name" Type="Str">noisefloor_Ptest,bg,132_.wav</Property>
				<Property Name="Source[109].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/noisefloor_Ptest,bg,132_.wav</Property>
				<Property Name="Source[109].type" Type="Str">File</Property>
				<Property Name="Source[11].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[11].name" Type="Str">Test3_Pcal,3,168_A1_ N184.wav</Property>
				<Property Name="Source[11].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,3,168_A1_ N184.wav</Property>
				<Property Name="Source[11].type" Type="Str">File</Property>
				<Property Name="Source[110].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[110].name" Type="Str">noisefloor_Ptest,bg,150_.wav</Property>
				<Property Name="Source[110].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/noisefloor_Ptest,bg,150_.wav</Property>
				<Property Name="Source[110].type" Type="Str">File</Property>
				<Property Name="Source[111].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[111].name" Type="Str">noisefloor_Ptest,bg,168_.wav</Property>
				<Property Name="Source[111].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/noisefloor_Ptest,bg,168_.wav</Property>
				<Property Name="Source[111].type" Type="Str">File</Property>
				<Property Name="Source[112].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[112].name" Type="Str">Test2_Pcal,1,132_.wav</Property>
				<Property Name="Source[112].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,1,132_.wav</Property>
				<Property Name="Source[112].type" Type="Str">File</Property>
				<Property Name="Source[113].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[113].name" Type="Str">Test2_Pcal,1,150_.wav</Property>
				<Property Name="Source[113].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,1,150_.wav</Property>
				<Property Name="Source[113].type" Type="Str">File</Property>
				<Property Name="Source[114].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[114].name" Type="Str">Test2_Pcal,1,168_.wav</Property>
				<Property Name="Source[114].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,1,168_.wav</Property>
				<Property Name="Source[114].type" Type="Str">File</Property>
				<Property Name="Source[115].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[115].name" Type="Str">Test2_Pcal,2,132_.wav</Property>
				<Property Name="Source[115].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,2,132_.wav</Property>
				<Property Name="Source[115].type" Type="Str">File</Property>
				<Property Name="Source[116].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[116].name" Type="Str">Test2_Pcal,2,150_.wav</Property>
				<Property Name="Source[116].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,2,150_.wav</Property>
				<Property Name="Source[116].type" Type="Str">File</Property>
				<Property Name="Source[117].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[117].name" Type="Str">Test2_Pcal,2,168_.wav</Property>
				<Property Name="Source[117].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,2,168_.wav</Property>
				<Property Name="Source[117].type" Type="Str">File</Property>
				<Property Name="Source[118].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[118].name" Type="Str">Test2_Pcal,3,132_.wav</Property>
				<Property Name="Source[118].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,3,132_.wav</Property>
				<Property Name="Source[118].type" Type="Str">File</Property>
				<Property Name="Source[119].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[119].name" Type="Str">Test2_Pcal,3,150_.wav</Property>
				<Property Name="Source[119].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,3,150_.wav</Property>
				<Property Name="Source[119].type" Type="Str">File</Property>
				<Property Name="Source[12].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[12].name" Type="Str">Test3_Pcal,4,132_A1_ N134.wav</Property>
				<Property Name="Source[12].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,4,132_A1_ N134.wav</Property>
				<Property Name="Source[12].type" Type="Str">File</Property>
				<Property Name="Source[120].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[120].name" Type="Str">Test2_Pcal,3,168_.wav</Property>
				<Property Name="Source[120].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,3,168_.wav</Property>
				<Property Name="Source[120].type" Type="Str">File</Property>
				<Property Name="Source[121].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[121].name" Type="Str">Test2_Pcal,4,132_.wav</Property>
				<Property Name="Source[121].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,4,132_.wav</Property>
				<Property Name="Source[121].type" Type="Str">File</Property>
				<Property Name="Source[122].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[122].name" Type="Str">Test2_Pcal,4,150_.wav</Property>
				<Property Name="Source[122].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,4,150_.wav</Property>
				<Property Name="Source[122].type" Type="Str">File</Property>
				<Property Name="Source[123].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[123].name" Type="Str">Test2_Pcal,4,168_.wav</Property>
				<Property Name="Source[123].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,4,168_.wav</Property>
				<Property Name="Source[123].type" Type="Str">File</Property>
				<Property Name="Source[124].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[124].name" Type="Str">Test2_Pcal,5,132_.wav</Property>
				<Property Name="Source[124].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,5,132_.wav</Property>
				<Property Name="Source[124].type" Type="Str">File</Property>
				<Property Name="Source[125].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[125].name" Type="Str">Test2_Pcal,5,150_.wav</Property>
				<Property Name="Source[125].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,5,150_.wav</Property>
				<Property Name="Source[125].type" Type="Str">File</Property>
				<Property Name="Source[126].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[126].name" Type="Str">Test2_Pcal,5,168_.wav</Property>
				<Property Name="Source[126].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,5,168_.wav</Property>
				<Property Name="Source[126].type" Type="Str">File</Property>
				<Property Name="Source[127].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[127].name" Type="Str">Test2_Pcal,6,132_.wav</Property>
				<Property Name="Source[127].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,6,132_.wav</Property>
				<Property Name="Source[127].type" Type="Str">File</Property>
				<Property Name="Source[128].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[128].name" Type="Str">Test2_Pcal,6,150_.wav</Property>
				<Property Name="Source[128].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,6,150_.wav</Property>
				<Property Name="Source[128].type" Type="Str">File</Property>
				<Property Name="Source[129].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[129].name" Type="Str">Test2_Pcal,6,168_.wav</Property>
				<Property Name="Source[129].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Pcal,6,168_.wav</Property>
				<Property Name="Source[129].type" Type="Str">File</Property>
				<Property Name="Source[13].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[13].name" Type="Str">Test3_Pcal,4,150_A1_ N154.wav</Property>
				<Property Name="Source[13].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,4,150_A1_ N154.wav</Property>
				<Property Name="Source[13].type" Type="Str">File</Property>
				<Property Name="Source[130].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[130].name" Type="Str">Test2_Ptest,1,1,132_.wav</Property>
				<Property Name="Source[130].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,1,1,132_.wav</Property>
				<Property Name="Source[130].type" Type="Str">File</Property>
				<Property Name="Source[131].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[131].name" Type="Str">Test2_Ptest,1,1,150_.wav</Property>
				<Property Name="Source[131].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,1,1,150_.wav</Property>
				<Property Name="Source[131].type" Type="Str">File</Property>
				<Property Name="Source[132].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[132].name" Type="Str">Test2_Ptest,1,1,168_.wav</Property>
				<Property Name="Source[132].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,1,1,168_.wav</Property>
				<Property Name="Source[132].type" Type="Str">File</Property>
				<Property Name="Source[133].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[133].name" Type="Str">Test2_Ptest,1,2,132_.wav</Property>
				<Property Name="Source[133].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,1,2,132_.wav</Property>
				<Property Name="Source[133].type" Type="Str">File</Property>
				<Property Name="Source[134].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[134].name" Type="Str">Test2_Ptest,1,2,150_.wav</Property>
				<Property Name="Source[134].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,1,2,150_.wav</Property>
				<Property Name="Source[134].type" Type="Str">File</Property>
				<Property Name="Source[135].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[135].name" Type="Str">Test2_Ptest,1,2,168_.wav</Property>
				<Property Name="Source[135].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,1,2,168_.wav</Property>
				<Property Name="Source[135].type" Type="Str">File</Property>
				<Property Name="Source[136].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[136].name" Type="Str">Test2_Ptest,2,1,132_.wav</Property>
				<Property Name="Source[136].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,2,1,132_.wav</Property>
				<Property Name="Source[136].type" Type="Str">File</Property>
				<Property Name="Source[137].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[137].name" Type="Str">Test2_Ptest,2,1,150_.wav</Property>
				<Property Name="Source[137].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,2,1,150_.wav</Property>
				<Property Name="Source[137].type" Type="Str">File</Property>
				<Property Name="Source[138].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[138].name" Type="Str">Test2_Ptest,2,1,168_.wav</Property>
				<Property Name="Source[138].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,2,1,168_.wav</Property>
				<Property Name="Source[138].type" Type="Str">File</Property>
				<Property Name="Source[139].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[139].name" Type="Str">Test2_Ptest,2,2,132_.wav</Property>
				<Property Name="Source[139].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,2,2,132_.wav</Property>
				<Property Name="Source[139].type" Type="Str">File</Property>
				<Property Name="Source[14].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[14].name" Type="Str">Test3_Pcal,4,168_A1_ N184.wav</Property>
				<Property Name="Source[14].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,4,168_A1_ N184.wav</Property>
				<Property Name="Source[14].type" Type="Str">File</Property>
				<Property Name="Source[140].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[140].name" Type="Str">Test2_Ptest,2,2,150_.wav</Property>
				<Property Name="Source[140].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,2,2,150_.wav</Property>
				<Property Name="Source[140].type" Type="Str">File</Property>
				<Property Name="Source[141].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[141].name" Type="Str">Test2_Ptest,2,2,168_.wav</Property>
				<Property Name="Source[141].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,2,2,168_.wav</Property>
				<Property Name="Source[141].type" Type="Str">File</Property>
				<Property Name="Source[142].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[142].name" Type="Str">Test2_Ptest,3,1,132_.wav</Property>
				<Property Name="Source[142].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,3,1,132_.wav</Property>
				<Property Name="Source[142].type" Type="Str">File</Property>
				<Property Name="Source[143].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[143].name" Type="Str">Test2_Ptest,3,1,150_.wav</Property>
				<Property Name="Source[143].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,3,1,150_.wav</Property>
				<Property Name="Source[143].type" Type="Str">File</Property>
				<Property Name="Source[144].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[144].name" Type="Str">Test2_Ptest,3,1,168_.wav</Property>
				<Property Name="Source[144].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,3,1,168_.wav</Property>
				<Property Name="Source[144].type" Type="Str">File</Property>
				<Property Name="Source[145].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[145].name" Type="Str">Test2_Ptest,3,2,132_.wav</Property>
				<Property Name="Source[145].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,3,2,132_.wav</Property>
				<Property Name="Source[145].type" Type="Str">File</Property>
				<Property Name="Source[146].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[146].name" Type="Str">Test2_Ptest,3,2,150_.wav</Property>
				<Property Name="Source[146].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,3,2,150_.wav</Property>
				<Property Name="Source[146].type" Type="Str">File</Property>
				<Property Name="Source[147].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[147].name" Type="Str">Test2_Ptest,3,2,168_.wav</Property>
				<Property Name="Source[147].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,3,2,168_.wav</Property>
				<Property Name="Source[147].type" Type="Str">File</Property>
				<Property Name="Source[148].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[148].name" Type="Str">Test2_Ptest,4,1,132_.wav</Property>
				<Property Name="Source[148].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,4,1,132_.wav</Property>
				<Property Name="Source[148].type" Type="Str">File</Property>
				<Property Name="Source[149].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[149].name" Type="Str">Test2_Ptest,4,1,150_.wav</Property>
				<Property Name="Source[149].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,4,1,150_.wav</Property>
				<Property Name="Source[149].type" Type="Str">File</Property>
				<Property Name="Source[15].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[15].name" Type="Str">Test3_Pcal,5,132_A1_ N144.wav</Property>
				<Property Name="Source[15].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,5,132_A1_ N144.wav</Property>
				<Property Name="Source[15].type" Type="Str">File</Property>
				<Property Name="Source[150].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[150].name" Type="Str">Test2_Ptest,4,1,168_.wav</Property>
				<Property Name="Source[150].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,4,1,168_.wav</Property>
				<Property Name="Source[150].type" Type="Str">File</Property>
				<Property Name="Source[151].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[151].name" Type="Str">Test2_Ptest,4,2,132_.wav</Property>
				<Property Name="Source[151].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,4,2,132_.wav</Property>
				<Property Name="Source[151].type" Type="Str">File</Property>
				<Property Name="Source[152].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[152].name" Type="Str">Test2_Ptest,4,2,150_.wav</Property>
				<Property Name="Source[152].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,4,2,150_.wav</Property>
				<Property Name="Source[152].type" Type="Str">File</Property>
				<Property Name="Source[153].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[153].name" Type="Str">Test2_Ptest,4,2,168_.wav</Property>
				<Property Name="Source[153].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,4,2,168_.wav</Property>
				<Property Name="Source[153].type" Type="Str">File</Property>
				<Property Name="Source[154].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[154].name" Type="Str">Test2_Ptest,5,1,132_.wav</Property>
				<Property Name="Source[154].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,5,1,132_.wav</Property>
				<Property Name="Source[154].type" Type="Str">File</Property>
				<Property Name="Source[155].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[155].name" Type="Str">Test2_Ptest,5,1,150_.wav</Property>
				<Property Name="Source[155].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,5,1,150_.wav</Property>
				<Property Name="Source[155].type" Type="Str">File</Property>
				<Property Name="Source[156].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[156].name" Type="Str">Test2_Ptest,5,1,168_.wav</Property>
				<Property Name="Source[156].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,5,1,168_.wav</Property>
				<Property Name="Source[156].type" Type="Str">File</Property>
				<Property Name="Source[157].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[157].name" Type="Str">Test2_Ptest,5,2,132_.wav</Property>
				<Property Name="Source[157].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,5,2,132_.wav</Property>
				<Property Name="Source[157].type" Type="Str">File</Property>
				<Property Name="Source[158].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[158].name" Type="Str">Test2_Ptest,5,2,150_.wav</Property>
				<Property Name="Source[158].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,5,2,150_.wav</Property>
				<Property Name="Source[158].type" Type="Str">File</Property>
				<Property Name="Source[159].dest" Type="Str">{1DB94BAB-0E5D-48A7-A0FB-10A158911F15}</Property>
				<Property Name="Source[159].name" Type="Str">Test2_Ptest,5,2,168_.wav</Property>
				<Property Name="Source[159].tag" Type="Ref">/My Computer/Installer Files/60 dB Attenuation Validation Set/Test2_Ptest,5,2,168_.wav</Property>
				<Property Name="Source[159].type" Type="Str">File</Property>
				<Property Name="Source[16].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[16].name" Type="Str">Test3_Pcal,5,150_A1_ N154.wav</Property>
				<Property Name="Source[16].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,5,150_A1_ N154.wav</Property>
				<Property Name="Source[16].type" Type="Str">File</Property>
				<Property Name="Source[17].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[17].name" Type="Str">Test3_Pcal,5,168_A1_ N184.wav</Property>
				<Property Name="Source[17].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,5,168_A1_ N184.wav</Property>
				<Property Name="Source[17].type" Type="Str">File</Property>
				<Property Name="Source[18].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[18].name" Type="Str">Test3_Pcal,6,132_A1_ N134.wav</Property>
				<Property Name="Source[18].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,6,132_A1_ N134.wav</Property>
				<Property Name="Source[18].type" Type="Str">File</Property>
				<Property Name="Source[19].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[19].name" Type="Str">Test3_Pcal,6,150_A1_ N154.wav</Property>
				<Property Name="Source[19].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,6,150_A1_ N154.wav</Property>
				<Property Name="Source[19].type" Type="Str">File</Property>
				<Property Name="Source[2].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[2].name" Type="Str">Test3_Pcal,1,132_A1_ N144.wav</Property>
				<Property Name="Source[2].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,1,132_A1_ N144.wav</Property>
				<Property Name="Source[2].type" Type="Str">File</Property>
				<Property Name="Source[20].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[20].name" Type="Str">Test3_Pcal,6,168_A1_ N184.wav</Property>
				<Property Name="Source[20].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,6,168_A1_ N184.wav</Property>
				<Property Name="Source[20].type" Type="Str">File</Property>
				<Property Name="Source[21].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[21].name" Type="Str">Test3_Ptest,1,1,132_A1_ N134.wav</Property>
				<Property Name="Source[21].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,1,1,132_A1_ N134.wav</Property>
				<Property Name="Source[21].type" Type="Str">File</Property>
				<Property Name="Source[22].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[22].name" Type="Str">Test3_Ptest,1,1,150_A1_ N144.wav</Property>
				<Property Name="Source[22].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,1,1,150_A1_ N144.wav</Property>
				<Property Name="Source[22].type" Type="Str">File</Property>
				<Property Name="Source[23].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[23].name" Type="Str">Test3_Ptest,1,1,168_A1_ N164.wav</Property>
				<Property Name="Source[23].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,1,1,168_A1_ N164.wav</Property>
				<Property Name="Source[23].type" Type="Str">File</Property>
				<Property Name="Source[24].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[24].name" Type="Str">Test3_Ptest,1,2,132_A1_ N134.wav</Property>
				<Property Name="Source[24].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,1,2,132_A1_ N134.wav</Property>
				<Property Name="Source[24].type" Type="Str">File</Property>
				<Property Name="Source[25].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[25].name" Type="Str">Test3_Ptest,1,2,150_A1_ N144.wav</Property>
				<Property Name="Source[25].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,1,2,150_A1_ N144.wav</Property>
				<Property Name="Source[25].type" Type="Str">File</Property>
				<Property Name="Source[26].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[26].name" Type="Str">Test3_Ptest,1,2,168_A1_ N164.wav</Property>
				<Property Name="Source[26].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,1,2,168_A1_ N164.wav</Property>
				<Property Name="Source[26].type" Type="Str">File</Property>
				<Property Name="Source[27].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[27].name" Type="Str">Test3_Ptest,2,1,132_A1_ N134.wav</Property>
				<Property Name="Source[27].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,2,1,132_A1_ N134.wav</Property>
				<Property Name="Source[27].type" Type="Str">File</Property>
				<Property Name="Source[28].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[28].name" Type="Str">Test3_Ptest,2,1,150_A1_ N144.wav</Property>
				<Property Name="Source[28].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,2,1,150_A1_ N144.wav</Property>
				<Property Name="Source[28].type" Type="Str">File</Property>
				<Property Name="Source[29].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[29].name" Type="Str">Test3_Ptest,2,1,168_A1_ N164.wav</Property>
				<Property Name="Source[29].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,2,1,168_A1_ N164.wav</Property>
				<Property Name="Source[29].type" Type="Str">File</Property>
				<Property Name="Source[3].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[3].name" Type="Str">Test3_Pcal,1,150_A1_ N154.wav</Property>
				<Property Name="Source[3].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,1,150_A1_ N154.wav</Property>
				<Property Name="Source[3].type" Type="Str">File</Property>
				<Property Name="Source[30].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[30].name" Type="Str">Test3_Ptest,2,2,132_A1_ N134.wav</Property>
				<Property Name="Source[30].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,2,2,132_A1_ N134.wav</Property>
				<Property Name="Source[30].type" Type="Str">File</Property>
				<Property Name="Source[31].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[31].name" Type="Str">Test3_Ptest,2,2,150_A1_ N144.wav</Property>
				<Property Name="Source[31].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,2,2,150_A1_ N144.wav</Property>
				<Property Name="Source[31].type" Type="Str">File</Property>
				<Property Name="Source[32].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[32].name" Type="Str">Test3_Ptest,2,2,168_A1_ N164.wav</Property>
				<Property Name="Source[32].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,2,2,168_A1_ N164.wav</Property>
				<Property Name="Source[32].type" Type="Str">File</Property>
				<Property Name="Source[33].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[33].name" Type="Str">Test3_Ptest,3,1,132_A1_ N134.wav</Property>
				<Property Name="Source[33].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,1,132_A1_ N134.wav</Property>
				<Property Name="Source[33].type" Type="Str">File</Property>
				<Property Name="Source[34].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[34].name" Type="Str">Test3_Ptest,3,1,150_A1_ N144.wav</Property>
				<Property Name="Source[34].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,1,150_A1_ N144.wav</Property>
				<Property Name="Source[34].type" Type="Str">File</Property>
				<Property Name="Source[35].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[35].name" Type="Str">Test3_Ptest,3,1,168_A1_ N164.wav</Property>
				<Property Name="Source[35].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,1,168_A1_ N164.wav</Property>
				<Property Name="Source[35].type" Type="Str">File</Property>
				<Property Name="Source[36].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[36].name" Type="Str">Test3_Ptest,3,2,132_A1_ N124.wav</Property>
				<Property Name="Source[36].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,2,132_A1_ N124.wav</Property>
				<Property Name="Source[36].type" Type="Str">File</Property>
				<Property Name="Source[37].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[37].name" Type="Str">Test3_Ptest,3,2,132_A1_ N134.wav</Property>
				<Property Name="Source[37].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,2,132_A1_ N134.wav</Property>
				<Property Name="Source[37].type" Type="Str">File</Property>
				<Property Name="Source[38].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[38].name" Type="Str">Test3_Ptest,3,2,150_A1_ N154.wav</Property>
				<Property Name="Source[38].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,2,150_A1_ N154.wav</Property>
				<Property Name="Source[38].type" Type="Str">File</Property>
				<Property Name="Source[39].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[39].name" Type="Str">Test3_Ptest,3,2,168_A1_ N164.wav</Property>
				<Property Name="Source[39].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,3,2,168_A1_ N164.wav</Property>
				<Property Name="Source[39].type" Type="Str">File</Property>
				<Property Name="Source[4].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[4].name" Type="Str">Test3_Pcal,1,168_A1_ N174.wav</Property>
				<Property Name="Source[4].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,1,168_A1_ N174.wav</Property>
				<Property Name="Source[4].type" Type="Str">File</Property>
				<Property Name="Source[40].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[40].name" Type="Str">Test3_Ptest,4,1,132_A1_ N134.wav</Property>
				<Property Name="Source[40].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,4,1,132_A1_ N134.wav</Property>
				<Property Name="Source[40].type" Type="Str">File</Property>
				<Property Name="Source[41].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[41].name" Type="Str">Test3_Ptest,4,1,150_A1_ N154.wav</Property>
				<Property Name="Source[41].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,4,1,150_A1_ N154.wav</Property>
				<Property Name="Source[41].type" Type="Str">File</Property>
				<Property Name="Source[42].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[42].name" Type="Str">Test3_Ptest,4,1,168_A1_ N164.wav</Property>
				<Property Name="Source[42].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,4,1,168_A1_ N164.wav</Property>
				<Property Name="Source[42].type" Type="Str">File</Property>
				<Property Name="Source[43].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[43].name" Type="Str">Test3_Ptest,4,2,132_A1_ N134.wav</Property>
				<Property Name="Source[43].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,4,2,132_A1_ N134.wav</Property>
				<Property Name="Source[43].type" Type="Str">File</Property>
				<Property Name="Source[44].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[44].name" Type="Str">Test3_Ptest,4,2,150_A1_ N144.wav</Property>
				<Property Name="Source[44].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,4,2,150_A1_ N144.wav</Property>
				<Property Name="Source[44].type" Type="Str">File</Property>
				<Property Name="Source[45].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[45].name" Type="Str">Test3_Ptest,4,2,168_A1_ N164.wav</Property>
				<Property Name="Source[45].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,4,2,168_A1_ N164.wav</Property>
				<Property Name="Source[45].type" Type="Str">File</Property>
				<Property Name="Source[46].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[46].name" Type="Str">Test3_Ptest,5,1,132_A1_ N134.wav</Property>
				<Property Name="Source[46].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,5,1,132_A1_ N134.wav</Property>
				<Property Name="Source[46].type" Type="Str">File</Property>
				<Property Name="Source[47].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[47].name" Type="Str">Test3_Ptest,5,1,150_A1_ N144.wav</Property>
				<Property Name="Source[47].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,5,1,150_A1_ N144.wav</Property>
				<Property Name="Source[47].type" Type="Str">File</Property>
				<Property Name="Source[48].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[48].name" Type="Str">Test3_Ptest,5,1,168_A1_ N164.wav</Property>
				<Property Name="Source[48].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,5,1,168_A1_ N164.wav</Property>
				<Property Name="Source[48].type" Type="Str">File</Property>
				<Property Name="Source[49].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[49].name" Type="Str">Test3_Ptest,5,2,132_A1_ N134.wav</Property>
				<Property Name="Source[49].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,5,2,132_A1_ N134.wav</Property>
				<Property Name="Source[49].type" Type="Str">File</Property>
				<Property Name="Source[5].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[5].name" Type="Str">Test3_Pcal,2,132_A1_ N134.wav</Property>
				<Property Name="Source[5].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,2,132_A1_ N134.wav</Property>
				<Property Name="Source[5].type" Type="Str">File</Property>
				<Property Name="Source[50].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[50].name" Type="Str">Test3_Ptest,5,2,150_A1_ N144.wav</Property>
				<Property Name="Source[50].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,5,2,150_A1_ N144.wav</Property>
				<Property Name="Source[50].type" Type="Str">File</Property>
				<Property Name="Source[51].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[51].name" Type="Str">Test3_Ptest,5,2,168_A1_ N164.wav</Property>
				<Property Name="Source[51].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Ptest,5,2,168_A1_ N164.wav</Property>
				<Property Name="Source[51].type" Type="Str">File</Property>
				<Property Name="Source[52].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[52].name" Type="Str">noisefloor_Pcal,bg,132_.wav</Property>
				<Property Name="Source[52].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/noisefloor_Pcal,bg,132_.wav</Property>
				<Property Name="Source[52].type" Type="Str">File</Property>
				<Property Name="Source[53].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[53].name" Type="Str">noisefloor_Pcal,bg,150_.wav</Property>
				<Property Name="Source[53].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/noisefloor_Pcal,bg,150_.wav</Property>
				<Property Name="Source[53].type" Type="Str">File</Property>
				<Property Name="Source[54].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[54].name" Type="Str">noisefloor_Pcal,bg,168_.wav</Property>
				<Property Name="Source[54].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/noisefloor_Pcal,bg,168_.wav</Property>
				<Property Name="Source[54].type" Type="Str">File</Property>
				<Property Name="Source[55].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[55].name" Type="Str">noisefloor_Ptest,bg,132_.wav</Property>
				<Property Name="Source[55].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/noisefloor_Ptest,bg,132_.wav</Property>
				<Property Name="Source[55].type" Type="Str">File</Property>
				<Property Name="Source[56].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[56].name" Type="Str">noisefloor_Ptest,bg,150_.wav</Property>
				<Property Name="Source[56].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/noisefloor_Ptest,bg,150_.wav</Property>
				<Property Name="Source[56].type" Type="Str">File</Property>
				<Property Name="Source[57].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[57].name" Type="Str">noisefloor_Ptest,bg,168_.wav</Property>
				<Property Name="Source[57].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/noisefloor_Ptest,bg,168_.wav</Property>
				<Property Name="Source[57].type" Type="Str">File</Property>
				<Property Name="Source[58].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[58].name" Type="Str">Test_Pcal,1,132_.wav</Property>
				<Property Name="Source[58].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,1,132_.wav</Property>
				<Property Name="Source[58].type" Type="Str">File</Property>
				<Property Name="Source[59].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[59].name" Type="Str">Test_Pcal,1,150_.wav</Property>
				<Property Name="Source[59].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,1,150_.wav</Property>
				<Property Name="Source[59].type" Type="Str">File</Property>
				<Property Name="Source[6].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[6].name" Type="Str">Test3_Pcal,2,150_A1_ N154.wav</Property>
				<Property Name="Source[6].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,2,150_A1_ N154.wav</Property>
				<Property Name="Source[6].type" Type="Str">File</Property>
				<Property Name="Source[60].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[60].name" Type="Str">Test_Pcal,1,168_.wav</Property>
				<Property Name="Source[60].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,1,168_.wav</Property>
				<Property Name="Source[60].type" Type="Str">File</Property>
				<Property Name="Source[61].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[61].name" Type="Str">Test_Pcal,2,132_.wav</Property>
				<Property Name="Source[61].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,2,132_.wav</Property>
				<Property Name="Source[61].type" Type="Str">File</Property>
				<Property Name="Source[62].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[62].name" Type="Str">Test_Pcal,2,150_.wav</Property>
				<Property Name="Source[62].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,2,150_.wav</Property>
				<Property Name="Source[62].type" Type="Str">File</Property>
				<Property Name="Source[63].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[63].name" Type="Str">Test_Pcal,2,168_.wav</Property>
				<Property Name="Source[63].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,2,168_.wav</Property>
				<Property Name="Source[63].type" Type="Str">File</Property>
				<Property Name="Source[64].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[64].name" Type="Str">Test_Pcal,3,132_.wav</Property>
				<Property Name="Source[64].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,3,132_.wav</Property>
				<Property Name="Source[64].type" Type="Str">File</Property>
				<Property Name="Source[65].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[65].name" Type="Str">Test_Pcal,3,150_.wav</Property>
				<Property Name="Source[65].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,3,150_.wav</Property>
				<Property Name="Source[65].type" Type="Str">File</Property>
				<Property Name="Source[66].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[66].name" Type="Str">Test_Pcal,3,168_.wav</Property>
				<Property Name="Source[66].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,3,168_.wav</Property>
				<Property Name="Source[66].type" Type="Str">File</Property>
				<Property Name="Source[67].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[67].name" Type="Str">Test_Pcal,4,132_.wav</Property>
				<Property Name="Source[67].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,4,132_.wav</Property>
				<Property Name="Source[67].type" Type="Str">File</Property>
				<Property Name="Source[68].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[68].name" Type="Str">Test_Pcal,4,150_.wav</Property>
				<Property Name="Source[68].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,4,150_.wav</Property>
				<Property Name="Source[68].type" Type="Str">File</Property>
				<Property Name="Source[69].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[69].name" Type="Str">Test_Pcal,4,168_.wav</Property>
				<Property Name="Source[69].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,4,168_.wav</Property>
				<Property Name="Source[69].type" Type="Str">File</Property>
				<Property Name="Source[7].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[7].name" Type="Str">Test3_Pcal,2,168_A1_ N184.wav</Property>
				<Property Name="Source[7].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,2,168_A1_ N184.wav</Property>
				<Property Name="Source[7].type" Type="Str">File</Property>
				<Property Name="Source[70].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[70].name" Type="Str">Test_Pcal,5,132_.wav</Property>
				<Property Name="Source[70].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,5,132_.wav</Property>
				<Property Name="Source[70].type" Type="Str">File</Property>
				<Property Name="Source[71].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[71].name" Type="Str">Test_Pcal,5,150_.wav</Property>
				<Property Name="Source[71].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,5,150_.wav</Property>
				<Property Name="Source[71].type" Type="Str">File</Property>
				<Property Name="Source[72].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[72].name" Type="Str">Test_Pcal,5,168_.wav</Property>
				<Property Name="Source[72].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,5,168_.wav</Property>
				<Property Name="Source[72].type" Type="Str">File</Property>
				<Property Name="Source[73].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[73].name" Type="Str">Test_Pcal,6,132_.wav</Property>
				<Property Name="Source[73].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,6,132_.wav</Property>
				<Property Name="Source[73].type" Type="Str">File</Property>
				<Property Name="Source[74].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[74].name" Type="Str">Test_Pcal,6,150_.wav</Property>
				<Property Name="Source[74].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,6,150_.wav</Property>
				<Property Name="Source[74].type" Type="Str">File</Property>
				<Property Name="Source[75].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[75].name" Type="Str">Test_Pcal,6,168_.wav</Property>
				<Property Name="Source[75].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Pcal,6,168_.wav</Property>
				<Property Name="Source[75].type" Type="Str">File</Property>
				<Property Name="Source[76].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[76].name" Type="Str">Test_Ptest,1,1,132_.wav</Property>
				<Property Name="Source[76].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,1,1,132_.wav</Property>
				<Property Name="Source[76].type" Type="Str">File</Property>
				<Property Name="Source[77].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[77].name" Type="Str">Test_Ptest,1,1,150_.wav</Property>
				<Property Name="Source[77].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,1,1,150_.wav</Property>
				<Property Name="Source[77].type" Type="Str">File</Property>
				<Property Name="Source[78].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[78].name" Type="Str">Test_Ptest,1,1,168_.wav</Property>
				<Property Name="Source[78].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,1,1,168_.wav</Property>
				<Property Name="Source[78].type" Type="Str">File</Property>
				<Property Name="Source[79].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[79].name" Type="Str">Test_Ptest,1,2,132_.wav</Property>
				<Property Name="Source[79].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,1,2,132_.wav</Property>
				<Property Name="Source[79].type" Type="Str">File</Property>
				<Property Name="Source[8].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[8].name" Type="Str">Test3_Pcal,3,132_A1_ N134.wav</Property>
				<Property Name="Source[8].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,3,132_A1_ N134.wav</Property>
				<Property Name="Source[8].type" Type="Str">File</Property>
				<Property Name="Source[80].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[80].name" Type="Str">Test_Ptest,1,2,150_.wav</Property>
				<Property Name="Source[80].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,1,2,150_.wav</Property>
				<Property Name="Source[80].type" Type="Str">File</Property>
				<Property Name="Source[81].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[81].name" Type="Str">Test_Ptest,1,2,168_.wav</Property>
				<Property Name="Source[81].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,1,2,168_.wav</Property>
				<Property Name="Source[81].type" Type="Str">File</Property>
				<Property Name="Source[82].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[82].name" Type="Str">Test_Ptest,2,1,132_.wav</Property>
				<Property Name="Source[82].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,2,1,132_.wav</Property>
				<Property Name="Source[82].type" Type="Str">File</Property>
				<Property Name="Source[83].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[83].name" Type="Str">Test_Ptest,2,1,150_.wav</Property>
				<Property Name="Source[83].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,2,1,150_.wav</Property>
				<Property Name="Source[83].type" Type="Str">File</Property>
				<Property Name="Source[84].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[84].name" Type="Str">Test_Ptest,2,1,168_.wav</Property>
				<Property Name="Source[84].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,2,1,168_.wav</Property>
				<Property Name="Source[84].type" Type="Str">File</Property>
				<Property Name="Source[85].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[85].name" Type="Str">Test_Ptest,2,2,132_.wav</Property>
				<Property Name="Source[85].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,2,2,132_.wav</Property>
				<Property Name="Source[85].type" Type="Str">File</Property>
				<Property Name="Source[86].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[86].name" Type="Str">Test_Ptest,2,2,150_.wav</Property>
				<Property Name="Source[86].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,2,2,150_.wav</Property>
				<Property Name="Source[86].type" Type="Str">File</Property>
				<Property Name="Source[87].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[87].name" Type="Str">Test_Ptest,2,2,168_.wav</Property>
				<Property Name="Source[87].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,2,2,168_.wav</Property>
				<Property Name="Source[87].type" Type="Str">File</Property>
				<Property Name="Source[88].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[88].name" Type="Str">Test_Ptest,3,1,132_.wav</Property>
				<Property Name="Source[88].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,3,1,132_.wav</Property>
				<Property Name="Source[88].type" Type="Str">File</Property>
				<Property Name="Source[89].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[89].name" Type="Str">Test_Ptest,3,1,150_.wav</Property>
				<Property Name="Source[89].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,3,1,150_.wav</Property>
				<Property Name="Source[89].type" Type="Str">File</Property>
				<Property Name="Source[9].dest" Type="Str">{D8EEAEDF-407A-4CC0-94F8-05D68745A5AD}</Property>
				<Property Name="Source[9].name" Type="Str">Test3_Pcal,3,132_A1_ N144.wav</Property>
				<Property Name="Source[9].tag" Type="Ref">/My Computer/Installer Files/Example Real Protector/Test3_Pcal,3,132_A1_ N144.wav</Property>
				<Property Name="Source[9].type" Type="Str">File</Property>
				<Property Name="Source[90].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[90].name" Type="Str">Test_Ptest,3,1,168_.wav</Property>
				<Property Name="Source[90].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,3,1,168_.wav</Property>
				<Property Name="Source[90].type" Type="Str">File</Property>
				<Property Name="Source[91].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[91].name" Type="Str">Test_Ptest,3,2,132_.wav</Property>
				<Property Name="Source[91].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,3,2,132_.wav</Property>
				<Property Name="Source[91].type" Type="Str">File</Property>
				<Property Name="Source[92].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[92].name" Type="Str">Test_Ptest,3,2,150_.wav</Property>
				<Property Name="Source[92].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,3,2,150_.wav</Property>
				<Property Name="Source[92].type" Type="Str">File</Property>
				<Property Name="Source[93].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[93].name" Type="Str">Test_Ptest,3,2,168_.wav</Property>
				<Property Name="Source[93].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,3,2,168_.wav</Property>
				<Property Name="Source[93].type" Type="Str">File</Property>
				<Property Name="Source[94].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[94].name" Type="Str">Test_Ptest,4,1,132_.wav</Property>
				<Property Name="Source[94].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,4,1,132_.wav</Property>
				<Property Name="Source[94].type" Type="Str">File</Property>
				<Property Name="Source[95].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[95].name" Type="Str">Test_Ptest,4,1,150_.wav</Property>
				<Property Name="Source[95].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,4,1,150_.wav</Property>
				<Property Name="Source[95].type" Type="Str">File</Property>
				<Property Name="Source[96].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[96].name" Type="Str">Test_Ptest,4,1,168_.wav</Property>
				<Property Name="Source[96].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,4,1,168_.wav</Property>
				<Property Name="Source[96].type" Type="Str">File</Property>
				<Property Name="Source[97].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[97].name" Type="Str">Test_Ptest,4,2,132_.wav</Property>
				<Property Name="Source[97].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,4,2,132_.wav</Property>
				<Property Name="Source[97].type" Type="Str">File</Property>
				<Property Name="Source[98].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[98].name" Type="Str">Test_Ptest,4,2,150_.wav</Property>
				<Property Name="Source[98].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,4,2,150_.wav</Property>
				<Property Name="Source[98].type" Type="Str">File</Property>
				<Property Name="Source[99].dest" Type="Str">{89C80649-C7A9-4F7A-AD9B-64C17DDCBA74}</Property>
				<Property Name="Source[99].name" Type="Str">Test_Ptest,4,2,168_.wav</Property>
				<Property Name="Source[99].tag" Type="Ref">/My Computer/Installer Files/10 dB Attenuation Validation Set/Test_Ptest,4,2,168_.wav</Property>
				<Property Name="Source[99].type" Type="Str">File</Property>
				<Property Name="SourceCount" Type="Int">160</Property>
			</Item>
		</Item>
	</Item>
</Project>
