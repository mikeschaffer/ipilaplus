<?xml version='1.0'?>
<Project Type="Project" LVVersion="8208000">
   <Item Name="My Computer" Type="My Computer">
      <Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.tcp.enabled" Type="Bool">false</Property>
      <Property Name="server.tcp.port" Type="Int">0</Property>
      <Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
      <Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="Main Demos" Type="Folder">
         <Item Name="1 - One dir intensity Analysis data from disk.vi" Type="VI" URL="1 - One dir intensity Analysis data from disk.vi"/>
         <Item Name="2 - Octave IntenPress.vi" Type="VI" URL="2 - Octave IntenPress.vi"/>
      </Item>
      <Item Name="sub-VIs" Type="Folder">
         <Item Name="svl_Check Data Valid [Integration] (N Ch) - modified.vi" Type="VI" URL="svl_Check Data Valid [Integration] (N Ch) - modified.vi"/>
         <Item Name="gras_Fractional-octave Analysis (N Ch).vi" Type="VI" URL="gras_Fractional-octave Analysis (N Ch).vi"/>
         <Item Name="gras_Fractional-octave Analysis with IC (linear avg).vi" Type="VI" URL="gras_Fractional-octave Analysis with IC (linear avg).vi"/>
         <Item Name="gras_Fractional-octave Analysis with IC (linear mode).vi" Type="VI" URL="gras_Fractional-octave Analysis with IC (linear mode).vi"/>
         <Item Name="gras_Fractional-octave Analysis with IC (peak mode).vi" Type="VI" URL="gras_Fractional-octave Analysis with IC (peak mode).vi"/>
         <Item Name="gras_Fractional-octave Analysis with IC.vi" Type="VI" URL="gras_Fractional-octave Analysis with IC.vi"/>
         <Item Name="SVL Integration (N Ch) - modified.vi" Type="VI" URL="SVL Integration (N Ch) - modified.vi"/>
         <Item Name="Fractional-octave time domain analysis (N Ch) (v2).vi" Type="VI" URL="Fractional-octave time domain analysis (N Ch) (v2).vi"/>
      </Item>
      <Item Name="Test Data" Type="Folder">
         <Item Name="skrald01.bin" Type="Document" URL="Test Data/skrald01.bin"/>
      </Item>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
